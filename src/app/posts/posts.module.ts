import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PostsRoutingModule } from './posts-routing.module';
import { PostsComponent } from './posts.component';
import { MatTableModule } from '@angular/material/table';
import { ViewPostsComponent } from './view-posts/view-posts.component';
import { CommentPostsComponent } from './comment-posts/comment-posts.component'
import {  ReactiveFormsModule, FormsModule } from "@angular/forms";
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSnackBarModule } from "@angular/material/snack-bar";
import {MatPaginatorModule} from '@angular/material/paginator';
@NgModule({
  declarations: [PostsComponent, ViewPostsComponent, CommentPostsComponent],
  imports: [
    CommonModule,
    PostsRoutingModule,
    MatTableModule,
    ReactiveFormsModule,
    FormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatSnackBarModule,
    MatPaginatorModule
    
  ]
})
export class PostsModule { }
