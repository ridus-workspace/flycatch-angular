import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTable, MatTableDataSource } from '@angular/material/table';
import { PostsService } from './posts.service';
import { Router } from "@angular/router";
import { MatPaginator } from '@angular/material/paginator';

import { MatChipsModule } from '@angular/material/chips';
import { Observable, merge } from "rxjs";
export interface PeriodicElement {
  title: string;
  description: string;

}




@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.scss']
})
export class PostsComponent implements OnInit {
  displayedColumns: string[] = ['title', 'description'];
  list: any;
  displayDataChanges: any;
  dataLength: number;
  // PAGINATOR
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;

  //  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  public dataSource = new MatTableDataSource<any | PeriodicElement>([]);
  constructor(private postsservice: PostsService, private route: Router) { }

  ngOnInit(): void {
  }
  ngAfterViewInit() {
    /**
     * Pagination
     */
    const displayDataChanges = [this.paginator.page];
    this.displayDataChanges = Observable;
    merge(...displayDataChanges).subscribe((val) => {
      this.listPosts();
    });
    /**
     *Call listPosts function after the page relod
     */
    this.listPosts();
  }
   /**
     * List All Posts(API)
     */
  listPosts() {
    this.postsservice
      .getPosts(this.paginator.pageSize, this.paginator.pageIndex + 1)
      .subscribe((res: any) => {

        const response = res['success'].data;
        console.log('responcee', response);
        this.list = response.data;
        this.dataLength = response.count
        this.dataSource = new MatTableDataSource(this.list);
      });

  }
   /**
     * navigate to selected post page
     */
    viewPost(id) {
    this.route.navigate(["/posts", id]);


  }
}
