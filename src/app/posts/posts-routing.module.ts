import { Component, NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PostsComponent } from './posts.component';
import { ViewPostsComponent } from './view-posts/view-posts.component'
import { CommentPostsComponent } from './comment-posts/comment-posts.component'

const routes: Routes = [
{
  path: "",
  component: PostsComponent,
},
{
  path:":id",
  component:ViewPostsComponent
},
{
  path:":id/comments",
  component:CommentPostsComponent
}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PostsRoutingModule { }
