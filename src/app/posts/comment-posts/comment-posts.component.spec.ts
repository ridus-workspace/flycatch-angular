import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommentPostsComponent } from './comment-posts.component';

describe('CommentPostsComponent', () => {
  let component: CommentPostsComponent;
  let fixture: ComponentFixture<CommentPostsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommentPostsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommentPostsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
