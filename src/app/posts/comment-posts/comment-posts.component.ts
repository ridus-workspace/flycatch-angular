import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup ,Validators,FormsModule,ReactiveFormsModule} from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSnackBar } from '@angular/material/snack-bar';
import * as moment from "moment";
// import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { ActivatedRoute, Router } from "@angular/router";
import{PostsService}from '../posts.service';
@Component({
  selector: 'app-comment-posts',
  templateUrl: './comment-posts.component.html',
  styleUrls: ['./comment-posts.component.scss']
})
export class CommentPostsComponent implements OnInit {
  data:any;
  postId:any;
  addCommentForm: FormGroup = new FormGroup({});
  constructor(private formBuilder: FormBuilder,private postservice:PostsService,private _snackBar: MatSnackBar,private actRoute:ActivatedRoute) {
    this.addCommentForm = formBuilder.group({
      name: [""],
      comment:[""],
      email: ["", [Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")]],

    })
   }

  ngOnInit(): void {
  }
  ngAfterViewInit(): void {
    //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
    //Add 'implements AfterViewInit' to the class.
    this.getPostComments();
  }
  /**
   * Get comments based on posts
   */
  getPostComments(){
  this.actRoute.paramMap.subscribe(params => {
    this.postId = params.get('id');
    this.postservice.getPostComments(this.postId).subscribe((res) => {
      console.log('this.dataressss',res)
      const response = res['success'].data;
      this.data = response;
      console.log('this.data',this.data)
      });
  });
  }
  /**
   * Change the date format
   * @param data 
   */
  getDate(data) {
    if (data !== null && data !== '' && data !== undefined) {
      let displayTime = null;     
        displayTime = moment(data).format('d-m-y');
        return displayTime;
      }
    return data
  }
  /**
   * Submiting form(save new command)
   */
  onSubmit(){
    const data = {
    "name": this.addCommentForm.value.name,
    "comment":this.addCommentForm.value.comment,
    "email": this.addCommentForm.value.email,
  }
  this.postservice.addPostComments(data,this.postId).subscribe(
    (response) => {
      if (response["success"].code === 200) {
        this.addCommentForm.reset() 
        this._snackBar.open(response["success"].message, "", {
          duration: 2000,
          verticalPosition: "bottom",
        });
        this.getPostComments();
      }
    },
    (error) => {
      const msg = "Invalid Form";
      this._snackBar.open(error["error"].error.message, "", {
        duration: 2000,
        verticalPosition: "bottom",
      });
    }
  );
}
}
