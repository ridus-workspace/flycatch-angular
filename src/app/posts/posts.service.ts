import { Injectable } from '@angular/core';
import { HttpClient,  HttpHeaders } from "@angular/common/http";
import { APP_URL } from "../../app/app.constant";
@Injectable({
  providedIn: 'root'
})
export class PostsService {
  apiUrl: any = APP_URL;
  
  constructor(private http: HttpClient) { }
  // GET post LIST
   getPosts(size, page) {
    return this.http.get(`${this.apiUrl}api/v1/posts/${size}/${page}`);
  }
  /**
   * Get Details of selected Post
   * @param id 
   */
  getDetails(id) {
    console.log(111121);
    return this.http.get(`${this.apiUrl}api/v1/posts/${id}`);
  }
  /**
   * Get comments of selected Post
   * @param id 
   */
  getPostComments(id){
    return this.http.get(`${this.apiUrl}api/v1/posts/${id}/comments`);
  }
  /*
  *@param id and Data 
  */
  addPostComments(data,id){
    return this.http.post(`${this.apiUrl}api/v1/posts/${id}/comments`,data);
  }
}
