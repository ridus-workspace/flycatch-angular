import { Component, OnInit } from '@angular/core';
import{PostsService}from '../posts.service';
import { ActivatedRoute, Router } from "@angular/router";
@Component({
  selector: 'app-view-posts',
  templateUrl: './view-posts.component.html',
  styleUrls: ['./view-posts.component.scss']
})
export class ViewPostsComponent implements OnInit {
data:any;
  constructor(private postsservice: PostsService,private route:Router,
    private actRoute: ActivatedRoute) { }

  ngOnInit(): void {
  }
  ngAfterViewInit(): void {
    //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
    //Add 'implements AfterViewInit' to the class.
    this.getPostDetails();
  }
  getPostDetails(){
    this.actRoute.paramMap.subscribe(params => {
    const id = params.get('id');
    this.postsservice.getDetails(id).subscribe((res) => {
      console.log('this.datares',res)
      const response = res['success'].data;
      this.data = response;
      console.log('this.data',this.data)
      });
  });
}
getComments(id){
  console.log("ds");
  this.route.navigate([`/posts/${id}/comments`]);
}

    

}
